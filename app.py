from flask import Flask
from jp.fonctionnalites import get_liste, get_dino


app = Flask(__name__)

from flask import render_template

@app.route('/')
def base():
    return render_template ('LesDinosaures.html', content=get_liste())
@app.route('/dinosaurs/<slug>')
def desc_dino(slug=None):
    return render_template("dino.html", dino=get_dino(slug))



