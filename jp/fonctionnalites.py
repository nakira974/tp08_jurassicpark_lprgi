import requests
import json

def get_liste():
    """
        Get slugs of dinosaures

        :param
        :param
        :return: list of dinosaures
        :rtype: json
        """

    reponse= requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/', )
    contenu= reponse.json()


    return contenu

def get_dino(slug):
    """
           Get content of dinosaures

           :param
           :param
           :return: content of dinosaures
           :rtype: json
           """

    reponse= requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/'+slug)
    contenu= reponse.json()
    return contenu

