from flask import url_for
import pytest



def test_base(client):
    response = client.get('/')
    assert 200 == response.status_code
    assert b'Hello World' in response.data


def test_content_dino(client):
    response = client.get(url_for('dinosaures', slug='parasaurolophus'))
    assert 200 == response.status_code
    assert b'' in response.data
    response = client.get(url_for('hello', name='dilophosaurus'))
    assert 200 == response.status_code
    assert b'Hello Sam' in response.data